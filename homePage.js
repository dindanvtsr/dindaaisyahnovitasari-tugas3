import React from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  Text,
  TouchableOpacity,
  Alert
} from 'react-native';

export default function HomePage({navigation}) {
  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={styles.content}>
          <TouchableOpacity
            style={styles.button}
            onPress={() => navigation.navigate('NetinfoPage')}>
            <Text style={styles.buttonTitle}>Net Info</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.button}
            onPress={() => navigation.navigate('DateTimePickerPage')}>
            <Text style={styles.buttonTitle}>Date Time Picker</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.button}
            onPress={() => navigation.navigate('SliderPage')}>
            <Text style={styles.buttonTitle}>Slider</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.button}
            onPress={() => navigation.navigate('GeoLocationPage')}>
            <Text style={styles.buttonTitle}>Geo Location</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.button}
            onPress={() => navigation.navigate('ProgressViewPage')}>
            <Text style={styles.buttonTitle}>Progress View</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.button}
            onPress={() => navigation.navigate('ProgressBarAndroidPage')}>
            <Text style={styles.buttonTitle}>Progress Bar Android</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.button}
            onPress={() => navigation.navigate('ClipboardPage')}>
            <Text style={styles.buttonTitle}>Clipboard</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.button}
            onPress={() => navigation.navigate('AsyncPage')}>
            <Text style={styles.buttonTitle}>Async Storage</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.button}
            onPress={() => navigation.navigate('CarouselPage')}>
            <Text style={styles.buttonTitle}>Carousel</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.button}
            onPress={() => navigation.navigate('ImageZoomViewer')}>
            <Text style={styles.buttonTitle}>Image Zoom Viewer</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.button}
            onPress={() => navigation.navigate('LinearGradientPage')}>
            <Text style={styles.buttonTitle}>Linear Gradient</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.button}
            onPress={() => navigation.navigate('RenderHtmlPage')}>
            <Text style={styles.buttonTitle}>Render Html</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.button}
            onPress={() => navigation.navigate('SharePage')}>
            <Text style={styles.buttonTitle}>Share</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.button}
            onPress={() => navigation.navigate('SkeletonPage1')}>
            <Text style={styles.buttonTitle}>Skeleton Placeholder</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.button}
            onPress={() => navigation.navigate('webViewPage')}>
            <Text style={styles.buttonTitle}>Web View</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.button}
            onPress={() => navigation.navigate('TooltipPage')}>
            <Text style={styles.buttonTitle}>RN Tooltip</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.button}
            onPress={() => navigation.navigate('IconsPage')}>
            <Text style={styles.buttonTitle}>Vector Icons</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F4F4F4',
  },
  content: {
    marginTop: 30,
    marginBottom: 40,
  },
  button: {
    backgroundColor: '#2E3283',
    borderRadius: 10,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 50,
    marginTop: 20
  },
  buttonTitle: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
  },
});
