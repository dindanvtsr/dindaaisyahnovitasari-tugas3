import React, { useState, useEffect } from 'react';
import { Text, View } from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder'; // Replace with the actual library you are using

function SkeletonPage1() {
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    setTimeout(() => {
      setIsLoading(false);
    }, 3000);
  }, []);

  return isLoading ? (
    <SkeletonPlaceholder>
      <SkeletonPlaceholder.Item width={200} height={14} marginLeft={20} marginTop={20}/>
      <SkeletonPlaceholder.Item width={200} height={14} marginTop={10} marginLeft={20}/>
      <SkeletonPlaceholder.Item width={200} height={14} marginTop={10} marginLeft={20}/>
    </SkeletonPlaceholder>
  ) : (
    <View>
      <Text style={{marginLeft:20, marginTop:20}}>Hello world!</Text>
      <Text style={{marginLeft:20}}>Hello world!</Text>
      <Text style={{marginLeft:20}}>Hello world!</Text>
    </View>
  );
}

export default SkeletonPage1;
