import React from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  Text,
  TouchableOpacity,
} from 'react-native';
import {ProgressView} from "@react-native-community/progress-view";

export default function ProgressViewPage() {
  return (
    <View style={styles.container}>
      <View style={styles.content}>
        <Text style={styles.title}>Progress View</Text>
        <ProgressView
          progressTintColor="orange"
          trackTintColor="blue"
          progress={0.5}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  title: {
    color: 'black',
    textAlign: 'center',
  }
});
