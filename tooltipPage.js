import React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import Tooltip from 'rn-tooltip';

const TooltipPage = () => {
  return (
    <View style={styles.container}>
      <Tooltip popover={<Text>Info here</Text>} backgroundColor='#FFF4B6'>
        <Text style={[{ backgroundColor: 'yellow', width: '100%', marginTop: 20}, styles.button]}>Press me</Text>
      </Tooltip>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    padding: 40,
  },
  button: {
    padding: 10,
    borderRadius: 4
  }
});

export default TooltipPage;