import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import ImageZoomViewer from './imageZoom';
import LinearGradientPage from './linearGradient';
import RenderHtmlPage from './renderHtmlPage';
import SharePage from './sharePage';
import SkeletonPage1 from './skeletonPage';
import webViewPage from './webViewPage';
import TooltipPage from './tooltipPage';
import IconsPage from './iconsPage';
import NetinfoPage from './netinfoPage';
import DateTimePickerPage from './dateTimePickerPage';
import SliderPage from './sliderPage';
import GeoLocationPage from './geoLocationPage';
import ProgressViewPage from './progressViewPage';
import ProgressBarAndroidPage from './progressBarAndroidPage';
import ClipboardPage from './clipboardPage';
import AsyncPage from './asyncPage';
import CarouselPage from './carouselPage';
import HomePage from './homePage';

const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerShown: true}}>
        <Stack.Screen name="HomePage" component={HomePage} options={{ title: 'Homepage' }}/>
        <Stack.Screen name="NetinfoPage" component={NetinfoPage} options={{ title: 'Net Info' }}/>
        <Stack.Screen name="DateTimePickerPage" component={DateTimePickerPage} options={{ title: 'Date Time Picker' }}/>
        <Stack.Screen name="SliderPage" component={SliderPage} options={{ title: 'Slider' }}/>
        <Stack.Screen name="GeoLocationPage" component={GeoLocationPage} options={{ title: 'Geo Location' }}/>
        <Stack.Screen name="ProgressViewPage" component={ProgressViewPage} options={{ title: 'Progress View' }}/>
        <Stack.Screen name="ProgressBarAndroidPage" component={ProgressBarAndroidPage} options={{ title: 'Progress Bar Android' }}/>
        <Stack.Screen name="ClipboardPage" component={ClipboardPage} options={{ title: 'Clipboard' }}/>
        <Stack.Screen name="AsyncPage" component={AsyncPage} options={{ title: 'Async Storage' }}/>
        <Stack.Screen name="CarouselPage" component={CarouselPage} options={{ title: 'Carousel' }}/>
        <Stack.Screen name="ImageZoomViewer" component={ImageZoomViewer} options={{ title: 'Image Zoom Viewer' }}/>
        <Stack.Screen name="LinearGradientPage" component={LinearGradientPage} options={{ title: 'Linear Gradient' }}/>
        <Stack.Screen name="RenderHtmlPage" component={RenderHtmlPage} options={{ title: 'Render HTML' }}/>
        <Stack.Screen name="SharePage" component={SharePage} options={{ title: 'Share' }}/>
        <Stack.Screen name="SkeletonPage1" component={SkeletonPage1} options={{ title: 'Skeleton Loading' }}/>
        <Stack.Screen name="webViewPage" component={webViewPage} options={{ title: 'Web View' }}/>
        <Stack.Screen name="TooltipPage" component={TooltipPage} options={{ title: 'RN Tooltip' }}/>
        <Stack.Screen name="IconsPage" component={IconsPage} options={{ title: 'Vector Icons' }}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}
